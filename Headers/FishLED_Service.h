/****************************************************************************

  Header file for Test Harness Service0
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef FishLED_Service_H
#define FishLED_Service_H

#include <stdint.h>
#include <stdbool.h>
#include "ES_Events.h"
//#include "MorseDecode.h"
//#include "MorseElements.h"
//#include "Button.h"
#include "BITDEFS.H"

typedef enum {InitFishWheel, 
               LedFish1, LedFish2, LedFish3, 
               FishButtonDown } FishWheelStates_t;
//Functions
bool InitalizeFishWheel(uint8_t Priority);
bool PostFishWheel(ES_Event_t ThisEvent);
bool CheckFishWheelEvents(void);
ES_Event_t RunFishWheel(ES_Event_t ThisEvent);

#endif
