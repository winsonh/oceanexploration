/****************************************************************************
 
  Header file for LEAFScanner 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef LEAFScanner_H
#define LEAFScanner_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitPState_LEAFScanner,
               LEAFScannerStandby,
               LEAFScannerActive} LEAFScannerState_t ;

// Public Function Prototypes
bool CheckLEAFInserted(void);
bool InitLEAFScannerSM(uint8_t);
bool PostLEAFScannerSM(ES_Event_t);
ES_Event_t RunLEAFScannerSM(ES_Event_t);

#endif /* CoralReefService_H */

