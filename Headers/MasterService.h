/****************************************************************************
 
  Header file for CoralReefService 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef MasterService_H
#define MasterService_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    

// typedefs for the states
// State definitions for use with the query function
typedef enum { LEAFOut, LEAFIn_ZeroWin, 
               LEAFIn_OneWin, LEAFIn_TwoWin, GameOver} MasterState_t ;

// Public Function Prototypes

bool InitMasterSM ( uint8_t Priority );
bool PostMasterSM( ES_Event_t ThisEvent );
ES_Event_t RunMasterSM( ES_Event_t ThisEvent );

#endif /* MasterService_H */

