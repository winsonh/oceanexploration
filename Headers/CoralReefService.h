/****************************************************************************
 
  Header file for CoralReefService 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef CoralReefService_H
#define CoralReefService_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitPState_CoralReef, FullColor, 
               ColorTransitioning, White } CoralReefState_t ;

// Public Function Prototypes

bool InitCoralReefSM ( uint8_t Priority );
bool PostCoralReefSM( ES_Event_t ThisEvent );
ES_Event_t RunCoralReefSM( ES_Event_t ThisEvent );
bool Check4TemperatureEvent(void);

#endif /* CoralReefService_H */

