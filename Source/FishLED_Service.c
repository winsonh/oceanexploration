/*FishLED_Service*/

#include "FishLED_Service.h"
#include <stdint.h>
#include <stdbool.h>
#include <termio.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "PWM16Tiva.h"
#include "MasterService.h"

/*TivaLibrary*/
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

/* include header files for the other modules in Lab3 that are referenced
*/
//#include "LCD_Write.h"
//#include "LCDService.h"


//#define ALL_BITS (0xff << 2)
#define ONE_SEC 1000

/*---------------------------- Module Variables ---------------------------*/
static uint8_t      MyPriority;
static FishWheelStates_t  CurrentState;
static int SelectedFish = 0;
static uint8_t LastButtonState;
static uint8_t LastLimitState;
static bool IsOceanExplorerGameOn = false;
static bool IsFishGameCompleted = false;

/*static uint8_t A2HI = BIT2HI;
static uint8_t A3HI = BIT3HI;
static uint8_t A4HI = BIT4HI;
static uint8_t A5HI = BIT5HI;
static uint8_t A6HI = BIT6HI;
//static uint8_t A7HI = BIT7HI;

static uint8_t A2LO = BIT2LO;
static uint8_t A3LO = BIT3LO;
static uint8_t A4LO = BIT4LO;
static uint8_t A5LO = BIT5LO;
static uint8_t A6LO = BIT6LO;
//static uint8_t A7LO = BIT7LO;


//static uint8_t C4HI = BIT4HI;
//static uint8_t C5HI = BIT5HI;
static uint8_t C6HI = BIT6HI;
//static uint8_t C7HI = BIT7HI;

static uint8_t C4LO = BIT4LO;
static uint8_t C5LO = BIT5LO;
static uint8_t C6LO = BIT6LO;
//static uint8_t C7LO = BIT7LO;
*/

//Function Prototypes
ES_Event_t RunFishWheel(ES_Event_t ThisEvent);
static void TurnOffAllFishLed(void);
static void TurnOnLED1(void);
static void TurnOnLED2(void);
static void TurnOnLED3(void);
static void TurnOnGreenLed(void); 
static void TurnOffGreenLed(void);
static void TurnOnRedLed(void);
static void TurnOffRedLed(void);
static void BuzzerOn(void);
static void BuzzerOff(void);
/*-----------------Functions-InitalizeFishWheel----------------*/

bool InitalizeFishWheel(uint8_t Priority)
{
  puts("Enter Fish Init");
//	printf("initialize\n");
//Takes a priority number, returns True.
  ES_Event_t ThisEvent;
//Initialize the MyPriority variable with the passed in parameter.
  MyPriority = Priority;

  //TERMIO_Init();
  //PA6 (10), PA7 (11), PC4 (6), PC5 (7)
  PWM_TIVA_SetDuty(100, 10);
  HWREG(SYSCTL_RCGCGPIO) |= (SYSCTL_RCGCGPIO_R0 | SYSCTL_RCGCGPIO_R2); //enable port A and C
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R2) != SYSCTL_PRGPIO_R2) //while loop for A
  {}

  //HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (A2HI|A3HI|A4HI|A5HI|A6HI);
  HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI | BIT6HI | BIT7HI);
  // enable digital bits 2-7 on port A
  //HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= (A2LO|A3LO|A4LO|A5LO|A6HI);
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= (BIT2LO & BIT3LO & BIT4LO & BIT5LO);
  //HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (BIT6HI | BIT7HI);
    PWM_TIVA_SetDuty(100, 10); //A6
    PWM_TIVA_SetDuty(100, 11); //A7
    //set  inputs for bits 2-5, and outputs for 6

  //HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= (C4HI|C5HI|C6HI);
  //  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= (BIT4HI | BIT5HI | BIT6HI);
    HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= (BIT6HI);
    HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= (BIT7HI);
    PWM_TIVA_SetDuty(100, 6); //C4
    PWM_TIVA_SetDuty(100, 7); //C5
  // enable digital bits 4-7 on port C
  //HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) |= (BIT4HI | BIT5HI | BIT6HI);
    HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) |= (BIT6HI);
    HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) |= (BIT7HI);
  //set  inputs for bits 4-7 output 
    
  //SetValues to 0
  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT2LO & BIT3LO & BIT4LO & BIT5LO);
      PWM_TIVA_SetDuty(0, 10); //A6
    PWM_TIVA_SetDuty(0, 11); //A7
    
  HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO & BIT5LO & BIT6LO);
   PWM_TIVA_SetDuty(0, 6); //C4
    PWM_TIVA_SetDuty(0, 7); //C5
    
  /*  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT7HI | BIT6HI);
    //puts("Set A bits High");
    
    HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT7LO & BIT6LO);
    //puts("Set A bits LO");
  
    HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT4HI | BIT5HI | C6HI);  
    //puts("Set C bits high");
    
    HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= (C4LO & C5LO & C6LO);  
    //puts("Set C bits Low");
  */  
  //  printf( "%d", HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)));
    
  LastLimitState  = HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT2HI | BIT3HI | BIT4HI);
  LastButtonState = HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT5HI);
  CurrentState = InitFishWheel;
    
  // Potentially wrong
    
   // CurrentState = InitFishWheel;

  ThisEvent.EventType = ES_INIT;

  if (ES_PostToService(MyPriority, ThisEvent) == 1)
  {
    printf("initialize posted\n");
    return 1;
  }
  else
  {
    return 0;
  }
}

/*----------------------Functions PostMorseElement-----------------------*/
bool PostFishWheel(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/*----------------------Functions CheckMorseEvents-----------------------*/




bool CheckFishWheelEvents(void)
{
  ES_Event_t  ThisEvent;
  bool        ReturnVal;
  ReturnVal = 0;
  
  uint8_t CurrentLimitState = HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT2HI | BIT3HI | BIT4HI);
  uint8_t CurrentButtonState = HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT5HI);
  
  if ((CurrentButtonState != LastButtonState) && (IsFishGameCompleted == false)) {

    if ((HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT5HI)) != 0) {
      BuzzerOn();
      ThisEvent.EventType =  ES_FISH_BUTTON_PRESS;
     ES_PostToService(MyPriority, ThisEvent);
     printf("\n\rFishLED_SM: Button Press Event Detected.");
    }
    else { //if (A5_val == A5LO)
     BuzzerOff();
     ThisEvent.EventType =  ES_FISH_BUTTON_UNPRESS;
     ES_PostToService(MyPriority, ThisEvent);
     printf("\n\rFishLED_SM: Button Release Event Detected.");
    }

    ES_Timer_StopTimer(TIMER_30s);
    ES_Timer_SetTimer(TIMER_30s, 30 * ONE_SEC);
    ES_Timer_StartTimer(TIMER_30s);
    printf("\n\rFishLED_SM: Button Event Resets the 30s Timer.\n");

    ReturnVal = 1;
  }
  

  else if ((CurrentLimitState != LastLimitState) && (IsFishGameCompleted == false) && (IsOceanExplorerGameOn == true)) {

    if ((HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT2HI))!= 0) {
     TurnOnLED1();
     ThisEvent.EventType =  ES_LIMIT_1;
     ES_PostToService(MyPriority, ThisEvent);
     printf("\n\rFishLED_SM: Limit Switch 1 Event Detected.");
    }
    else if ((HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT3HI)) != 0) {
     TurnOnLED2();
     ThisEvent.EventType =  ES_LIMIT_2;
     ES_PostToService(MyPriority, ThisEvent);
     printf("\n\rFishLED_SM: Limit Switch 2 Event Detected.");
    }
    else if ((HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT4HI)) != 0) {
     TurnOnLED3();
     ThisEvent.EventType =  ES_LIMIT_3;
     ES_PostToService(MyPriority, ThisEvent);
     printf("\n\rFishLED_SM: Limit Switch 3 Event Detected.");
    }

    ES_Timer_StopTimer(TIMER_30s);
    ES_Timer_SetTimer(TIMER_30s, 30 * ONE_SEC);
    ES_Timer_StartTimer(TIMER_30s);
    printf("\n\rFishLED_SM:Limit Switch Event Resets the 30s Timer.\n");

    ReturnVal = 1;
    
  }
  LastLimitState = CurrentLimitState;
  LastButtonState = CurrentButtonState;
  return ReturnVal;
}




















static void TurnOffAllFishLed(void){
  PWM_TIVA_SetDuty(0, 10); //A6
  PWM_TIVA_SetDuty(0, 11); //A7
  PWM_TIVA_SetDuty(0, 6); //C4
}

static void BuzzerOn (void){
HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT7HI);
}

static void BuzzerOff (void){
HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT7LO);
}

static void TurnOnLED1(void){
  TurnOffAllFishLed();
  PWM_TIVA_SetDuty(100, 10); //A6
}

static void TurnOnLED2(void){
  TurnOffAllFishLed();
  PWM_TIVA_SetDuty(100, 11);
}

static void TurnOnLED3(void){
  TurnOffAllFishLed();
  PWM_TIVA_SetDuty(100, 6);
}

static void TurnOnGreenLed(void){
  PWM_TIVA_SetDuty(100, 7);
}

static void TurnOffGreenLed(void){
  HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT5LO);
  PWM_TIVA_SetDuty(0, 7);
}

static void TurnOnRedLed(void){
  printf("turn on red light \r\n");
  HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT6HI); //test red
  printf ("red should be def on \r\n");
}

static void TurnOffRedLed(void){
  HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT6LO);
}




















ES_Event_t RunFishWheel(ES_Event_t ThisEvent){
  //puts ("Enter Run");
  
  ES_Event_t NoEventReturn;
  //static FishWheelStates_t  CurrentState;  
  FishWheelStates_t  NextState;
  NextState = CurrentState;
  // IsOceanExplorerGameOn = true; //added by nandini remove later when integrated with leaf
  switch (CurrentState)
    {
      case InitFishWheel:
        
      {
        /*
        *****************
        */
        SelectedFish = 0;
        TurnOffGreenLed();
        //puts(" Past selected Fish \n");
        /*
        *****************
        */
        
        if(ThisEvent.EventType == ES_GAME_ON) {
          IsOceanExplorerGameOn = true;
          PWM_TIVA_SetDuty(100, 10);
          PWM_TIVA_SetDuty(100, 11);
          PWM_TIVA_SetDuty(100, 6);
          printf("\n\rFishLED_SM (InitFishWheel state) receives ES_GAME_ON event");
          printf("\n\rFishLED_SM (InitFishWheel state) sets IsOceanExplorerGameOn = true\n");
        }
        
        if(ThisEvent.EventType == ES_GAME_OVER) {
          IsOceanExplorerGameOn = false;
          printf("\n\rFishLED_SM (InitFishWheel state) receives ES_GAME_OVER event");
          printf("\n\rFishLED_SM (InitFishWheel state) sets IsOceanExplorerGameOn = false\n");
        }
        
        /*
        *****************
        */
        if(ThisEvent.EventType == ES_SYSTEM_RESET) {
          IsOceanExplorerGameOn = false;
          IsFishGameCompleted = false;
          TurnOffAllFishLed();
          TurnOffGreenLed();
          TurnOffRedLed();
          printf("\n\rFishLED_SM (InitFishWheel state) receives ES_SYSTEM_RESET event");
          printf("\n\rFishLED_SM (InitFishWheel state) sets IsOceanExplorerGameOn = false, IsFishGameCompleted == false\n");
        }
        /*
        *****************
        */
        
        //printf("\n\r is ocean explorer game on? %d \n\r is fish game over? %d", IsFishGameCompleted);
        
        if ( (ThisEvent.EventType == ES_FISH_BUTTON_PRESS) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = FishButtonDown;
          printf("\n\rFishLED_SM (InitFishWheel state) receives ES_FISH_BUTTON_PRESS");
          printf("\n\rFishLED_SM (InitFishWheel state) NextState = FishButtonDown\n");
        }
        
        if ( (ThisEvent.EventType == ES_LIMIT_1) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = LedFish1;
          printf("\n\rFishLED_SM (InitFishWheel state) receives ES_LIMIT_1");
          printf("\n\rFishLED_SM (InitFishWheel state) NextState = LedFish1\n");
        }
        
        if ((ThisEvent.EventType == ES_LIMIT_2) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = LedFish2;
          printf("\n\rFishLED_SM (InitFishWheel state) receives ES_LIMIT_2");
          printf("\n\rFishLED_SM (InitFishWheel state) NextState = LedFish2\n");
        }
        
        if ((ThisEvent.EventType == ES_LIMIT_3) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = LedFish3;
          printf("\n\rFishLED_SM (InitFishWheel state) receives ES_LIMIT_3");
          printf("\n\rFishLED_SM (InitFishWheel state) NextState = LedFish3\n");
        }
      }
      break;
      
      
      case LedFish1:
      {
        TurnOffRedLed();


        
        /*
        *****************
        */
        SelectedFish = 1;
        /*
        *****************
        */
        
        if(ThisEvent.EventType == ES_GAME_OVER) {
          IsOceanExplorerGameOn = false;
          printf("\n\rFishLED_SM (LedFish1 state) receives ES_GAME_OVER event");
          printf("\n\rFishLED_SM (LedFish1 state) sets IsOceanExplorerGameOn = false\n");
        }

        /*
        *****************
        */
        if(ThisEvent.EventType == ES_SYSTEM_RESET) {
          IsOceanExplorerGameOn = false;
          IsFishGameCompleted = false;
          TurnOffAllFishLed();
          TurnOffGreenLed();
          TurnOffRedLed();
					NextState = InitFishWheel;
          printf("\n\rFishLED_SM (LedFish1 state) receives ES_SYSTEM_RESET event");
          printf("\n\rFishLED_SM (LedFish1 state) sets IsOceanExplorerGameOn = false, IsFishGameCompleted == false\n");
        }
        /*
        *****************
        */
        
        if ( (ThisEvent.EventType == ES_FISH_BUTTON_PRESS) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = FishButtonDown;
          printf("\n\rFishLED_SM (LedFish1 state) receives ES_FISH_BUTTON_PRESS event");
          printf("\n\rFishLED_SM (LedFish1 state) enters FishButtonDown state\n");
        }
        
        if ( (ThisEvent.EventType == ES_LIMIT_2) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = LedFish2;
          printf("\n\rFishLED_SM (LedFish1 state) receives ES_LIMIT_2 event");
          printf("\n\rFishLED_SM (LedFish1 state) enters LedFish2 state\n");
        }
        
        if ( (ThisEvent.EventType == ES_LIMIT_3) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = LedFish3;
          printf("\n\rFishLED_SM (LedFish1 state) receives ES_LIMIT_3 event");
          printf("\n\rFishLED_SM (LedFish1 state) enters LedFish3 state\n");
        }
        
        if ( (ThisEvent.EventType == ES_LIMIT_1) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = LedFish1;
          printf("\n\rFishLED_SM (LedFish1 state) receives ES_LIMIT_1 event");
          printf("\n\rFishLED_SM (LedFish1 state) enters LedFish1 state\n");
        }
      }
      break;
      
      
      case LedFish2:
      {
        TurnOffRedLed();
        
        /*
        *****************
        */
        //TurnOnLED2();
        SelectedFish = 2;
        /*
        *****************
        */
        
        if(ThisEvent.EventType == ES_GAME_OVER) {
          IsOceanExplorerGameOn = false;
          printf("\n\rFishLED_SM (LedFish2 state) receives ES_GAME_OVER event");
          printf("\n\rFishLED_SM (LedFish2 state) sets IsOceanExplorerGameOn = false\n");
        }
        
        /*
        *****************
        */
        if(ThisEvent.EventType == ES_SYSTEM_RESET) {
          IsOceanExplorerGameOn = false;
          IsFishGameCompleted = false;
          TurnOffAllFishLed();
          TurnOffGreenLed();
          TurnOffRedLed();
					NextState = InitFishWheel;
          printf("\n\rFishLED_SM (LedFish2 state) receives ES_SYSTEM_RESET event");
          printf("\n\rFishLED_SM (LedFish2 state) sets IsOceanExplorerGameOn = false, IsFishGameCompleted == false\n");
        }
        /*
        *****************
        */
        
        if ((ThisEvent.EventType == ES_FISH_BUTTON_PRESS) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = FishButtonDown;
          printf("\n\rFishLED_SM (LedFish2 state) receives ES_FISH_BUTTON_PRESS event");
          printf("\n\rFishLED_SM (LedFish2 state) enters FishButtonDown\n");
        }
        
        if ((ThisEvent.EventType == ES_LIMIT_1) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = LedFish1;
          printf("\n\rFishLED_SM (LedFish2 state) receives ES_LIMIT_1 event");
          printf("\n\rFishLED_SM (LedFish2 state) enters LedFish1\n");
        }
        
        if ((ThisEvent.EventType == ES_LIMIT_3) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = LedFish3;
          printf("\n\rFishLED_SM (LedFish2 state) receives ES_LIMIT_3 event");
          printf("\n\rFishLED_SM (LedFish2 state) enters LedFish3\n");
        }
        if ((ThisEvent.EventType == ES_LIMIT_2) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = LedFish2;
          printf("\n\rFishLED_SM (LedFish2 state) receives ES_LIMIT_2 event");
          printf("\n\rFishLED_SM (LedFish2 state) enters LedFish2\n");
        }
      }
      break;
      
      
      case LedFish3:
      {
        TurnOffRedLed();
        
        /*
        *****************
        */        
        //TurnOnLED3();
        SelectedFish = 3;
        /*
        *****************
        */
        
        if(ThisEvent.EventType == ES_GAME_OVER) {
          IsOceanExplorerGameOn = false;
          printf("\n\rFishLED_SM (LedFish3 state) receives ES_GAME_OVER event");
          printf("\n\rFishLED_SM (LedFish3 state) sets IsOceanExplorerGameOn = false\n");
        }
        
        /*
        *****************
        */
        if(ThisEvent.EventType == ES_SYSTEM_RESET) {
          IsOceanExplorerGameOn = false;
          IsFishGameCompleted = false;
          TurnOffAllFishLed();
          TurnOffGreenLed();
          TurnOffRedLed();
					NextState = InitFishWheel;
          printf("\n\rFishLED_SM (LedFish3 state) receives ES_SYSTEM_RESET event");
          printf("\n\rFishLED_SM (LedFish3 state) sets IsOceanExplorerGameOn = false, IsFishGameCompleted == false\n");
        }
        /*
        *****************
        */
        
        if ((ThisEvent.EventType == ES_FISH_BUTTON_PRESS) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = FishButtonDown;
          printf("\n\rFishLED_SM (LedFish3 state) receives ES_FISH_BUTTON_PRESS event");
          printf("\n\rFishLED_SM (LedFish3 state) enters FishButtonDown state \n");
        }
        
        if ((ThisEvent.EventType == ES_LIMIT_2) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = LedFish2;
          printf("\n\rFishLED_SM (LedFish3 state) receives ES_LIMIT_2 event");
          printf("\n\rFishLED_SM (LedFish3 state) enters LedFish2 state \n");
        }
        
        if ((ThisEvent.EventType == ES_LIMIT_1) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = LedFish1;
          printf("\n\rFishLED_SM (LedFish3 state) receives ES_LIMIT_1 event");
          printf("\n\rFishLED_SM (LedFish3 state) enters LedFish1 state \n");
        }
        
        if ((ThisEvent.EventType == ES_LIMIT_3)  && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          NextState = LedFish3;
          printf("\n\rFishLED_SM (LedFish3 state) receives ES_LIMIT_3 event");
          printf("\n\rFishLED_SM (LedFish3 state) enters LedFish3 state \n");
        }
      }
      break;
      
      
      case FishButtonDown:
      {
        
        if(ThisEvent.EventType == ES_GAME_OVER) {
          IsOceanExplorerGameOn = false;
          printf("\n\rFishLED_SM (FishButtonDown state) receives ES_GAME_OVER event");
          printf("\n\rFishLED_SM (FishButtonDown state) sets IsOceanExplorerGameOn = false\n");
        }
        
        /*
        *****************
        */
        if(ThisEvent.EventType == ES_SYSTEM_RESET) {
          IsOceanExplorerGameOn = false;
          IsFishGameCompleted = false;
          TurnOffAllFishLed();
          TurnOffGreenLed();
          TurnOffRedLed();
          printf("\n\rFishLED_SM (FishButtonDown state) receives ES_SYSTEM_RESET event");
          printf("\n\rFishLED_SM (FishButtonDown state) sets IsOceanExplorerGameOn = false, IsFishGameCompleted == false\n");
        }
        /*
        *****************
        */
        
        // If no fish is selected and the button is pressed,
        if ((SelectedFish == 0)  && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          if (ThisEvent.EventType == ES_FISH_BUTTON_UNPRESS)
          {
            NextState = InitFishWheel;
            printf("\n\rFishLED_SM (FishButtonDown state, SelectedFish = 0) receives ES_FISH_BUTTON_UNPRESS event");
            printf("\n\rFishLED_SM (FishButtonDown state, SelectedFish = 0) eneters InitFishWheel state\n");
          }
        }
        
        // If fish 1 is selected and the button is pressed,
        if ((SelectedFish == 1)  && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          TurnOnRedLed();
          if (ThisEvent.EventType == ES_FISH_BUTTON_UNPRESS)
          {
            //TurnOffRedLed();
            NextState = LedFish1;
            printf("\n\rFishLED_SM (FishButtonDown state, SelectedFish = 1) receives ES_FISH_BUTTON_UNPRESS event");
            printf("\n\rFishLED_SM (FishButtonDown state, SelectedFish = 1) TurnOnRedLed()");
            printf("\n\rFishLED_SM (FishButtonDown state, SelectedFish = 1) eneters LedFish1 state\n");
          }
        }
        
        // If fish 2 is selected and the button is pressed,
        if ((SelectedFish == 2) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          TurnOnRedLed();
          if (ThisEvent.EventType == ES_FISH_BUTTON_UNPRESS)
          {
            //TurnOffRedLed();
            NextState = LedFish2;
            printf("\n\rFishLED_SM (FishButtonDown state, SelectedFish = 2) receives ES_FISH_BUTTON_UNPRESS event");
            printf("\n\rFishLED_SM (FishButtonDown state, SelectedFish = 2) TurnOnRedLed()");
            printf("\n\rFishLED_SM (FishButtonDown state, SelectedFish = 2) eneters LedFish2 state\n");
          }
        }
        
        // If fish 3 (right answer) is selected and the button is pressed,
        if ((SelectedFish == 3) && (IsOceanExplorerGameOn == true) && (IsFishGameCompleted == false) ) {
          TurnOnGreenLed(); //WINNER
          IsFishGameCompleted = true;
          
          ES_Event_t ES_FISH_GAME_WIN_Event;
          ES_FISH_GAME_WIN_Event.EventType = ES_FISH_GAME_WIN;
          PostMasterSM(ES_FISH_GAME_WIN_Event);
          printf("\n\rFishLED_SM (FishButtonDown state, SelectedFish = 3) sends ES_FISH_GAME_WIN to MasterSM");
          
          //PostFishGameWon;
          if (ThisEvent.EventType == ES_FISH_BUTTON_UNPRESS) {
            //TurnOffGreenLed();
            NextState = InitFishWheel;
            printf("\n\rFishLED_SM (FishButtonDown state, SelectedFish = 3) receives ES_FISH_BUTTON_UNPRESS event");
            printf("\n\rFishLED_SM (FishButtonDown state, SelectedFish = 3) eneters InitFishWheel state\n");

          }
        }
      }
      break;
    }
    CurrentState = NextState;
    //puts("changing current state to next state \r\n");
    NoEventReturn.EventType = ES_NO_EVENT;
    //ThisEvent.EventType = ES_NO_EVENT;
    return NoEventReturn;
}