/****************************************************************************
 Module
   MasterService.c
 Description

 Author
   Winson Huang
****************************************************************************/

// #define TEST

/*----------------------------- Include Files -----------------------------*/
/* include header files for this service
*/
#include "MasterService.h"

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "ADMulti.h"
#include "PWM16Tiva.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

/* include header files for the other modules in the final project that are referenced
*/
#include "LEAFScanner.h"
#include "CoralReefService.h"
#include "Spirometer.h"
#include "SunService.h"
#include "FishLED_Service.h"


/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define NUM_PWM_PINS 16
#define NUM_AD_PINS 4

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void HWInit_Master(void);
static void PostToAllSubServices(ES_Event_t ThisEvent);
static uint8_t GetGamesWonNum(uint8_t binary);
static void DanceAndStamp(void);


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static MasterState_t CurrentState = LEAFOut;
// static ES_Event_t DeferralQueue[3+1];
static uint8_t GameWonRecord = 0; // 0; // Bit0: Fish Matching. Bit1: Coral Reef. Bit2: Whale Blow 
static bool SystemResetSent = false;
// static bool RESET_DELAY_TIMER_Active = false;
static bool WhaleMotorRunning = false;
/*------------------------------ Module Code ------------------------------*/





/****************************************************************************
 Function
     InitMasterSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
   This function initializes the required data for the Master service.

 Author
     Winson Huang, 11/12/18, 16:44
****************************************************************************/
bool InitMasterSM ( uint8_t Priority )
{ 
  MyPriority = Priority;
  CurrentState = LEAFOut;
	
	ES_Timer_SetTimer(WELCOME_BREAK_TIMER, 1 * ONE_SEC);
	ES_Timer_StartTimer(WELCOME_BREAK_TIMER);
	
  // Hardware initialization
  HWInit_Master();
  
  ES_Event_t ThisEvent;
  ThisEvent.EventType = ES_INIT;
  return PostMasterSM(ThisEvent);
}




static void HWInit_Master() {
  
  // Initialize Port A, PortB, PortC
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0; // Enable Port A
  while ( (HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R0) != SYSCTL_PRGPIO_R0){;}
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1; // Enable Port B
  while ( (HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1){;}
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2; // Enable Port C
  while ( (HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R2) != SYSCTL_PRGPIO_R2){;}
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R3; // Enable Port D
  while ( (HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R2) != SYSCTL_PRGPIO_R2){;}
  
  // Digital Pin Selection
  HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI); // Set PA2, PA3, PA4, PA5 to be digital
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT0HI | BIT2HI | BIT6HI); // Set PB0, PB2, PB6 to be digital
  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= (BIT6HI); // Set PC6 to be digital
  HWREG(GPIO_PORTD_BASE + GPIO_O_DEN) |= (BIT1HI); // Set PD1 to be digital
    
  // Data Direction Setting
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= (BIT2LO & BIT3LO & BIT4LO & BIT5LO); // Set PA2, PA3, PA4, PA5 to be input
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= (BIT0LO); // Set PB0 to be input
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT2HI | BIT6HI); // Set PB2, PB6 to be output
  HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) |= (BIT6HI); // Set PC6 to be output
  HWREG(GPIO_PORTD_BASE + GPIO_O_DIR) |= (BIT1HI); // Set PC6 to be digital
    
  // Initialize all 16 PWM Pins
  PWM_TIVA_Init(NUM_PWM_PINS); 
  
  // Initialize all 4 A-to-D Pins
  ADC_MultiInit(NUM_AD_PINS);
  
}

















ES_Event_t RunMasterSM(ES_Event_t ThisEvent) {
  struct ES_Event ReturnEvent;
  
  switch (CurrentState) {
    
    // LEAFOut state
    case LEAFOut:
      
			if( (ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == WELCOME_BREAK_TIMER)) {
				
				ES_Timer_StopTimer(WELCOME_BREAK_TIMER);
				ES_Timer_SetTimer(WELCOME_DANCE_TIMER, 3 * ONE_SEC);
				ES_Timer_StartTimer(WELCOME_DANCE_TIMER);
				
				PWM_TIVA_SetDuty(100, 13); // Turn on PF1 with PWM
				printf("\n\rMasterSM (LEAFOut state) receives WELCOME_BREAK_TIMER");
				printf("\n\rMasterSM (LEAFOut state) turns Tube Guy on\n");
			}
			
			if( (ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == WELCOME_DANCE_TIMER)) {
				ES_Timer_StopTimer(WELCOME_DANCE_TIMER);
				ES_Timer_SetTimer(WELCOME_BREAK_TIMER, 15 * ONE_SEC);
				ES_Timer_StartTimer(WELCOME_BREAK_TIMER);
				
				PWM_TIVA_SetDuty(0, 13); // Turn off PF1 with PWM
			  printf("\n\rMasterSM (LEAFOut state) receives WELCOME_DANCE_TIMER");
				printf("\n\rMasterSM (LEAFOut state) turns Tube Guy off\n");
			}			
		
      if( (ThisEvent.EventType == ES_RTD_AT_ROOM_TEMP) && (SystemResetSent == false)) {      
        ES_Event_t SystemResetEvent; 
        SystemResetEvent.EventType = ES_SYSTEM_RESET;
        SystemResetSent = true;
        
        PostMasterSM(SystemResetEvent);
        // PostLEAFScannerSM(SysResetEvent); // Not Done
        PostToAllSubServices(SystemResetEvent);
        printf("\n\rMasterSM (LEAFOut state) receives ES_RTD_AT_ROOM_TEMP");
        printf("\n\rMasterSM (LEAFOut state) sends ES_SYSTEM_RESET to PostToAllSubServices\n");        
      }
      
      if(ThisEvent.EventType == ES_SYSTEM_RESET) {
        GameWonRecord = 0;
        CurrentState = LEAFOut;
        PWM_TIVA_SetDuty(0, 13); // Turn off PF1
        PWM_TIVA_SetDuty(0, 0); // Turn off PB6 with PWM
        printf("\n\rMasterSM (LEAFOut state) receives ES_SYSTEM_RESET");
        printf("\n\rMasterSM (LEAFOut state) turns off Tube Guy (PB1)");
        printf("\n\rMasterSM (LEAFOut state) turns off solonoid (PB6)");
        printf("\n\rMasterSM (LEAFOut state) stays in LEAFOut state\n");
        
        ES_Timer_SetTimer(WELCOME_BREAK_TIMER, 1 * ONE_SEC);
        ES_Timer_StartTimer(WELCOME_BREAK_TIMER);
        
      }
      
      if(ThisEvent.EventType == ES_LEAF_INSERTION) {
        ES_Timer_StopTimer(WELCOME_BREAK_TIMER);
				ES_Timer_StopTimer(WELCOME_DANCE_TIMER);
				
				// Begin the 30s Timer
        printf("\n\r30s Timer starts");
        ES_Timer_SetTimer(TIMER_30s, 30 * ONE_SEC);
        ES_Timer_StartTimer(TIMER_30s);
        
        // Begin the 60s Timer
        printf("\n\r60s Timer starts");
        ES_Timer_SetTimer(TIMER_60s, 60 * ONE_SEC);
        ES_Timer_StartTimer(TIMER_60s);
        
        CurrentState = LEAFIn_ZeroWin;
        
        PostCoralReefSM(ThisEvent);
        
        struct ES_Event SystemResetEvent;
        SystemResetEvent.EventType = ES_SYSTEM_RESET;
        PostMasterSM(SystemResetEvent);
        // PostLEAFScannerSM(SysResetEvent); // Not Done
        PostToAllSubServices(SystemResetEvent);      
        
        struct ES_Event GameOnEvent;
        GameOnEvent.EventType = ES_GAME_ON;
        PostToAllSubServices(GameOnEvent);
        
        printf("\n\rMasterSM (LEAFOut state) receives ES_LEAF_INSERTION");
        printf("\n\rMasterSM (LEAFOut state) sends ES_GAME_ON to PostToAllSubServices");
        // printf("\n\rMasterSM (LEAFOut state) sends ES_SYSTEM_RESET to PostToAllSubServices");
        printf("\n\rMasterSM (LEAFOut state) enters LEAFIn_ZeroWin state\n");
      }
      
      break;

      
    // LEAFIn_ZeroWin state 
    case LEAFIn_ZeroWin:
      if(ThisEvent.EventType == ES_TIMEOUT && ((ThisEvent.EventParam == TIMER_30s) || (ThisEvent.EventParam == TIMER_60s))) {
        ES_Timer_StopTimer(TIMER_30s);
        ES_Timer_StopTimer(TIMER_60s);
        CurrentState = GameOver;
        
        ES_Event_t GameOverEvent; 
        GameOverEvent.EventType = ES_GAME_OVER;
        PostToAllSubServices(GameOverEvent);
        
        printf("\n\rMasterSM (LEAFIn_ZeroWin state) receives %d ES_TIMEOUT", ThisEvent.EventParam);
        printf("\n\rMasterSM (LEAFIn_ZeroWin State) stops TIMER_30s and TIMER_60s");
        printf("\n\rMasterSM (LEAFIn_ZeroWin State) enters GameOver state");
        printf("\n\rMasterSM (LEAFIn_ZeroWin State) sends ES_GAME_OVER to PostToAllSubServices\n");
        
        // Turn off all fish lights
        PWM_TIVA_SetDuty(0, 10); //A6
        PWM_TIVA_SetDuty(0, 11); //A7
        PWM_TIVA_SetDuty(0, 6); //C4
        
        // Turn off Coral Reef
        PWM_TIVA_SetDuty(0, 4);
        PWM_TIVA_SetDuty(0, 5);
      }
      
      if(ThisEvent.EventType == ES_LEAF_REMOVAL) {
        ES_Timer_StopTimer(TIMER_30s);
        ES_Timer_StopTimer(TIMER_60s);

        ES_Timer_SetTimer(RESET_DELAY_TIMER, 5 * ONE_SEC);
        ES_Timer_StartTimer(RESET_DELAY_TIMER);
        // RESET_DELAY_TIMER_Active = true;

        CurrentState = GameOver;
        PostCoralReefSM(ThisEvent);
        
        ES_Event_t GameOverEvent; 
        GameOverEvent.EventType = ES_GAME_OVER;
        PostToAllSubServices(GameOverEvent);
        
        printf("\n\rMasterSM (LEAFIn_ZeroWin state) receives ES_LEAF_REMOVAL");
        printf("\n\rMasterSM (LEAFIn_ZeroWin State) stops TIMER_30s and TIMER_60s");
        printf("\n\rMasterSM (LEAFIn_ZeroWin State) starts 5-s RESET_DELAY_TIMER");
        printf("\n\rMasterSM (LEAFIn_ZeroWin State) sends ES_LEAF_REMOVAL to PostCoralReefSM");
        printf("\n\rMasterSM (LEAFIn_ZeroWin State) enters GameOver state");
        printf("\n\rMasterSM (LEAFIn_ZeroWin State) sends ES_GAME_OVER to PostToAllSubServices\n");
        
        // Turn off all fish lights
        PWM_TIVA_SetDuty(0, 10); //A6
        PWM_TIVA_SetDuty(0, 11); //A7
        PWM_TIVA_SetDuty(0, 6); //C4
        
        // Turn off Coral Reef
        PWM_TIVA_SetDuty(0, 4);
        PWM_TIVA_SetDuty(0, 5);
      }
      
      if(ThisEvent.EventType == ES_FISH_GAME_WIN) {
        if( (GameWonRecord & BIT0HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT0HI;
          CurrentState = LEAFIn_OneWin;
          printf("\n\rMasterSM (LEAFIn_ZeroWin state) receives ES_FISH_GAME_WIN, GameWonRecord = %d",GetGamesWonNum(GameWonRecord));
          printf("\n\rMasterSM (LEAFIn_ZeroWin State) enters LEAFIn_OneWin state\n");
        }
      }
      
      if(ThisEvent.EventType == ES_CORALREEF_GAME_WIN) {
        if( (GameWonRecord & BIT1HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT1HI;
          CurrentState = LEAFIn_OneWin;
          printf("\n\rMasterSM (LEAFIn_ZeroWin state) receives ES_CORALREEF_GAME_WIN, GameWonRecord = %d",GetGamesWonNum(GameWonRecord));
          printf("\n\rMasterSM (LEAFIn_ZeroWin State) enters LEAFIn_OneWin state\n");
        }
      }
      
      if(ThisEvent.EventType == ES_WHALE_GAME_WIN) {
        if( (GameWonRecord & BIT2HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT2HI;
          CurrentState = LEAFIn_OneWin;
          WhaleMotorRunning = true;
          printf("\n\rMasterSM (LEAFIn_ZeroWin state) receives ES_WHALE_GAME_WIN, GameWonRecord = %d",GetGamesWonNum(GameWonRecord));
          printf("\n\rMasterSM (LEAFIn_ZeroWin State) enters LEAFIn_OneWin state\n");
        }
      }
      break;
    
    
    // LEAFIn_OneWin state 
    case LEAFIn_OneWin:
      
      if(ThisEvent.EventType == ES_TIMEOUT && ((ThisEvent.EventParam == TIMER_30s) || (ThisEvent.EventParam == TIMER_60s))) {
        ES_Timer_StopTimer(TIMER_30s);
        ES_Timer_StopTimer(TIMER_60s);
        CurrentState = GameOver;
        
        ES_Event_t GameOverEvent; 
        GameOverEvent.EventType = ES_GAME_OVER;
        PostToAllSubServices(GameOverEvent);
        printf("\n\rMasterSM (LEAFIn_OneWin state) receives %d ES_TIMEOUT", ThisEvent.EventParam);
        printf("\n\rMasterSM (LEAFIn_OneWin State) stops TIMER_30s and TIMER_60s");
        printf("\n\rMasterSM (LEAFIn_OneWin State) enters GameOver state");
        printf("\n\rMasterSM (LEAFIn_OneWin State) sends ES_GAME_OVER to PostToAllSubServices\n");
        
        // Turn off all fish lights
        PWM_TIVA_SetDuty(0, 10); //A6
        PWM_TIVA_SetDuty(0, 11); //A7
        PWM_TIVA_SetDuty(0, 6); //C4

        // Turn off Coral Reef
        PWM_TIVA_SetDuty(0, 4);
        PWM_TIVA_SetDuty(0, 5);
      }
      
      if(ThisEvent.EventType == ES_LEAF_REMOVAL) {
        ES_Timer_StopTimer(TIMER_30s);
        ES_Timer_StopTimer(TIMER_60s);

        ES_Timer_SetTimer(RESET_DELAY_TIMER, 5 * ONE_SEC);
        ES_Timer_StartTimer(RESET_DELAY_TIMER);
        // RESET_DELAY_TIMER_Active = true;

        CurrentState = GameOver;
        PostCoralReefSM(ThisEvent);
        
        ES_Event_t GameOverEvent; 
        GameOverEvent.EventType = ES_GAME_OVER;
        PostToAllSubServices(GameOverEvent);
        
        printf("\n\rMasterSM (LEAFIn_OneWin state) receives ES_LEAF_REMOVAL");
        printf("\n\rMasterSM (LEAFIn_OneWin State) stops TIMER_30s and TIMER_60s");
        printf("\n\rMasterSM (LEAFIn_OneWin State) starts 5-s RESET_DELAY_TIMER");
        printf("\n\rMasterSM (LEAFIn_OneWin State) sends ES_GAME_OVER to PostCoralReefSM");
        printf("\n\rMasterSM (LEAFIn_OneWin State) enters GameOver state");
        printf("\n\rMasterSM (LEAFIn_OneWin State) sends ES_GAME_OVER to PostToAllSubServices\n");
        
        // Turn off all fish lights
        PWM_TIVA_SetDuty(0, 10); //A6
        PWM_TIVA_SetDuty(0, 11); //A7
        PWM_TIVA_SetDuty(0, 6); //C4
        
        // Turn off Coral Reef
        PWM_TIVA_SetDuty(0, 4);
        PWM_TIVA_SetDuty(0, 5);
      }
      
      if(ThisEvent.EventType == ES_FISH_GAME_WIN) {
        if( (GameWonRecord & BIT0HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT0HI;
          CurrentState = LEAFIn_TwoWin;
          printf("\n\rMasterSM (LEAFIn_OneWin state) receives ES_FISH_GAME_WIN, GameWonRecord = %d",GetGamesWonNum(GameWonRecord));
          printf("\n\rMasterSM (LEAFIn_OneWin State) enters LEAFIn_TwoWin state\n");
        }
      }
      
      if(ThisEvent.EventType == ES_CORALREEF_GAME_WIN) {
        if( (GameWonRecord & BIT1HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT1HI;
          CurrentState = LEAFIn_TwoWin;
          printf("\n\rMasterSM (LEAFIn_OneWin state) receives ES_CORALREEF_GAME_WIN, GameWonRecord = %d",GetGamesWonNum(GameWonRecord));
          printf("\n\rMasterSM (LEAFIn_OneWin State) enters LEAFIn_TwoWin state\n");
        }
      }
      
      if(ThisEvent.EventType == ES_WHALE_GAME_WIN) {
        if( (GameWonRecord & BIT2HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT2HI;
          CurrentState = LEAFIn_TwoWin;
          WhaleMotorRunning = true;
          printf("\n\rMasterSM (LEAFIn_OneWin state) receives ES_WHALE_GAME_WIN, GameWonRecord = %d",GetGamesWonNum(GameWonRecord));
          printf("\n\rMasterSM (LEAFIn_OneWin State) enters LEAFIn_TwoWin state\n");
        }
      }
      
      if(ThisEvent.EventType == ES_WHALE_MOTOR_STOP) {
        WhaleMotorRunning = false;
        printf("\n\rMasterSM (LEAFIn_OneWin State) receives ES_WHALE_MOTOR_STOP");
        printf("\n\rMasterSM sets WhaleMotorRunning = false");
      }
               
      break;
    
    
    // LEAFIn_TwoWin state
    case LEAFIn_TwoWin:
      if( (ThisEvent.EventType == ES_TIMEOUT) && ((ThisEvent.EventParam == TIMER_30s) || (ThisEvent.EventParam == TIMER_60s))) {
        ES_Timer_StopTimer(TIMER_30s);
        ES_Timer_StopTimer(TIMER_60s);
        CurrentState = GameOver;
        
        ES_Event_t GameOverEvent; 
        GameOverEvent.EventType = ES_GAME_OVER;
        PostToAllSubServices(GameOverEvent);
        
        printf("\n\rIn LEAFIn_TwoWin state, MasterSM receives %ds ES_TIMEOUT", ThisEvent.EventParam);
        printf("\n\rMasterSM (LEAFIn_TwoWin state) receives %d ES_TIMEOUT", ThisEvent.EventParam);
        printf("\n\rMasterSM (LEAFIn_TwoWin State) stops TIMER_30s and TIMER_60s");
        printf("\n\rMasterSM (LEAFIn_TwoWin State) enters GameOver state");
        printf("\n\rMasterSM (LEAFIn_TwoWin State) sends ES_GAME_OVER to PostToAllSubServices\n");
        
        // Turn off all fish lights
        PWM_TIVA_SetDuty(0, 10); //A6
        PWM_TIVA_SetDuty(0, 11); //A7
        PWM_TIVA_SetDuty(0, 6); //C4
        
        // Turn off Coral Reef
        PWM_TIVA_SetDuty(0, 4);
        PWM_TIVA_SetDuty(0, 5);
      }
      
      if(ThisEvent.EventType == ES_LEAF_REMOVAL) {
        printf("\n\rIn LEAFIn_TwoWin state, ES_LEAF_REMOVAL");
        ES_Timer_StopTimer(TIMER_30s);
        ES_Timer_StopTimer(TIMER_60s);

        ES_Timer_SetTimer(RESET_DELAY_TIMER, 5 * ONE_SEC);
        ES_Timer_StartTimer(RESET_DELAY_TIMER);
        // RESET_DELAY_TIMER_Active = true;

        CurrentState = GameOver;
        PostCoralReefSM(ThisEvent);
        
        ES_Event_t GameOverEvent; 
        GameOverEvent.EventType = ES_GAME_OVER;
        PostToAllSubServices(GameOverEvent);
        
        printf("\n\rMasterSM (LEAFIn_TwoWin state) receives ES_LEAF_REMOVAL");
        printf("\n\rMasterSM (LEAFIn_TwoWin State) stops TIMER_30s and TIMER_60s");
        printf("\n\rMasterSM (LEAFIn_TwoWin State) starts 5s RESET_DELAY_TIMER");
        printf("\n\rMasterSM (LEAFIn_TwoWin State) enters GameOver state");
        printf("\n\rMasterSM (LEAFIn_TwoWin State) sends ES_GAME_OVER to PostToAllSubServices\n");
        
        // Turn off all fish lights
        PWM_TIVA_SetDuty(0, 10); //A6
        PWM_TIVA_SetDuty(0, 11); //A7
        PWM_TIVA_SetDuty(0, 6); //C4
        
        // Turn off Coral Reef
        PWM_TIVA_SetDuty(0, 4);
        PWM_TIVA_SetDuty(0, 5);
      }      
      
      if(ThisEvent.EventType == ES_FISH_GAME_WIN) {
        if( (GameWonRecord & BIT0HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT0HI;
          CurrentState = GameOver;
          ES_Timer_StopTimer(TIMER_30s);
          ES_Timer_StopTimer(TIMER_60s);
          
          ES_Event_t GameOverEvent; 
          GameOverEvent.EventType = ES_GAME_OVER;
          PostToAllSubServices(GameOverEvent);
          
          printf("\n\rMasterSM (LEAFIn_TwoWin state) receives ES_FISH_GAME_WIN, GameWonRecord = %d",GetGamesWonNum(GameWonRecord));
          printf("\n\rMasterSM (LEAFIn_TwoWin State) stops TIMER_30s and TIMER_60s");
          printf("\n\rMasterSM (LEAFIn_TwoWin State) enters GameOver state");
          if(WhaleMotorRunning == false) {
            DanceAndStamp();
            printf("\n\rMasterSM (LEAFIn_TwoWin State) does DanceAndStamp() celetration");
          }
          printf("\n\rMasterSM (LEAFIn_TwoWin State) sends ES_GAME_OVER to PostToAllSubServices\n");
        }
      }
      
      if(ThisEvent.EventType == ES_CORALREEF_GAME_WIN) {
        if( (GameWonRecord & BIT1HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT1HI;
          CurrentState = GameOver;
          ES_Timer_StopTimer(TIMER_30s);
          ES_Timer_StopTimer(TIMER_60s);
          
          ES_Event_t GameOverEvent; 
          GameOverEvent.EventType = ES_GAME_OVER;
          PostToAllSubServices(GameOverEvent);
          
          printf("\n\rMasterSM (LEAFIn_TwoWin state) receives ES_CORALREEF_GAME_WIN, GameWonRecord = %d",GetGamesWonNum(GameWonRecord));
          printf("\n\rMasterSM (LEAFIn_TwoWin State) stops TIMER_30s and TIMER_60s");
          printf("\n\rMasterSM (LEAFIn_TwoWin State) enters GameOver state");
          if(WhaleMotorRunning == false) {
            DanceAndStamp();
            printf("\n\rMasterSM (LEAFIn_TwoWin State) does DanceAndStamp() celetration");
          }
          printf("\n\rMasterSM (LEAFIn_TwoWin State) sends ES_GAME_OVER to PostToAllSubServices\n");
        }
      }
      
      if(ThisEvent.EventType == ES_WHALE_GAME_WIN) {
        if( (GameWonRecord & BIT2HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT2HI;
          CurrentState = GameOver;
          WhaleMotorRunning = true;
          
          ES_Event_t GameOverEvent; 
          GameOverEvent.EventType = ES_GAME_OVER;
          // PostToAllSubServices(GameOverEvent);
          PostCoralReefSM(GameOverEvent); // Not Done
          PostSunService(GameOverEvent); // Not Done
          PostFishWheel(GameOverEvent); // Not Done
          PostSunService(GameOverEvent);

          printf("\n\rMasterSM (LEAFIn_TwoWin state) receives ES_WHALE_GAME_WIN, GameWonRecord = %d",GetGamesWonNum(GameWonRecord));
          printf("\n\rMasterSM (LEAFIn_TwoWin State) stops TIMER_30s and TIMER_60s");
          printf("\n\rMasterSM (LEAFIn_TwoWin State) enters GameOver state");
          printf("\n\rMasterSM (LEAFIn_TwoWin State) does DanceAndStamp() celetration");
          printf("\n\rMasterSM (LEAFIn_TwoWin State) sends ES_GAME_OVER to PostCoralReefSM, PostSunService, PostFishWheel, PostSunService\n");
        }
      }
      
      if(ThisEvent.EventType == ES_WHALE_MOTOR_STOP) {
        WhaleMotorRunning = false;
        printf("\n\rMasterSM (LEAFIn_TwoWin State) receives ES_WHALE_MOTOR_STOP");
        printf("\n\rMasterSM sets WhaleMotorRunning = false");
      }
      
      break;
    

    // LEAFIn_AllWon state        
    case GameOver:
    
      if((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == TUBE_GUY_TIMER) ) {
        PWM_TIVA_SetDuty(0, 13); // Turn off PF1 with PWM
        // HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT1LO; // Turn off PB1
        printf("\n\rMasterSM (GameOver State) receives TUBE_GUY_TIMER.");
        printf("\n\rMasterSM (GameOver State) sets PWM_TIVA_SetDuty(0, 13)");
      }
      
      if((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == STAMPER_TIMER) ) {
        PWM_TIVA_SetDuty(0, 0); // Turn off PB6 with PWM
        // HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT6LO; // Turn off PB6
        printf("\n\rMasterSM (GameOver State) receives STAMPER_TIMER.");
        printf("\n\rMasterSM (GameOver State) sets PWM_TIVA_SetDuty(0, 0).\n");
      }
      
      if(ThisEvent.EventType == ES_LEAF_REMOVAL) {
        PostCoralReefSM(ThisEvent);
        
        ES_Timer_SetTimer(RESET_DELAY_TIMER, 1 * ONE_SEC);
        ES_Timer_StartTimer(RESET_DELAY_TIMER);
        // RESET_DELAY_TIMER_Active = true;
        
        // We begin system reset 5 seconds after the LEAF is pulled out.
        printf("\n\rMasterSM (GameOver state) receives ES_LEAF_REMOVAL");
        printf("\n\rMasterSM (GameOver State) sends ES_LEAF_REMOVAL to PostCoralReefSM");
        printf("\n\rMasterSM (GameOver State) starts a 5s Timer for system reset");

        // Turn off all fish lights
        PWM_TIVA_SetDuty(0, 10); //A6
        PWM_TIVA_SetDuty(0, 11); //A7
        PWM_TIVA_SetDuty(0, 6); //C4
        
        // Turn off Coral Reef
        PWM_TIVA_SetDuty(0, 4);
        PWM_TIVA_SetDuty(0, 5);
      } 
      
      if((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == RESET_DELAY_TIMER)) {
        CurrentState = LEAFOut;
        SystemResetSent = false;
        // RESET_DELAY_TIMER_Active = false;
        printf("\n\rMasterSM (GameOver state) receives %d ES_TIMEOUT", ThisEvent.EventParam);
        printf("\n\rMasterSM (GameOver State) enters LEAFOut state");
      }
      
      if(ThisEvent.EventType == ES_WHALE_MOTOR_STOP) {
        WhaleMotorRunning = false;
        DanceAndStamp();
        printf("\n\rMasterSM (GameOver State) receives ES_WHALE_MOTOR_STOP");
        printf("\n\rMasterSM (GameOver State) DanceAndStamp()");
      }

      break;
  }
  
  ReturnEvent.EventType = ES_NO_EVENT;
  return ReturnEvent;
}































/****************************************************************************
 Function
     PostMasterSM

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue

 Author
     Winson Huang, 11/12/18, 16:52
****************************************************************************/
bool PostMasterSM( ES_Event_t ThisEvent )
{ 
  return ES_PostToService( MyPriority, ThisEvent);
}



static void PostToAllSubServices(ES_Event_t ThisEvent) {
  PostCoralReefSM(ThisEvent);
  PostSunService(ThisEvent);
  PostSpirometerSM(ThisEvent);
  PostFishWheel(ThisEvent);
}





static uint8_t GetGamesWonNum(uint8_t binary) {
  uint8_t GamesWonNum = 0;
  
  if(binary & BIT0HI) {
    GamesWonNum = GamesWonNum + 1;
  }
  if(binary & BIT1HI) {
    GamesWonNum = GamesWonNum + 1;
  }
  if(binary & BIT2HI) {
    GamesWonNum = GamesWonNum + 1;
  }
  
  return GamesWonNum;
}








static void DanceAndStamp(void) {
  // Tube Guy Dance
  printf("\n\rTube Guy dance starts");
  ES_Timer_SetTimer(TUBE_GUY_TIMER, 10 * ONE_SEC);
  ES_Timer_StartTimer(TUBE_GUY_TIMER);
  PWM_TIVA_SetDuty(100, 13); // Turn on PF1 with PWM
  // HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT1HI; // Turn on PB1

  // Stamp LEAF
  printf("\n\rStamper enlongs\r");
  ES_Timer_SetTimer(STAMPER_TIMER, 2 * ONE_SEC);
  ES_Timer_StartTimer(STAMPER_TIMER);
  PWM_TIVA_SetDuty(100, 0); // Turn on PB6 with PWM
  // HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT6HI; // Turn on PB6    
}
 












































/****************************************************************************
                                Test Harness
****************************************************************************/
#ifdef TEST
#include <termio.h>

int main(void) {
  TERMIO_Init();
  // printf("TEST NOW!\n");
  InitCoralReefService (MyPriority);
  
  printf("\n\rTEST NOW!");
  printf("\n\rTEST NOW!");
  printf("\n\rTEST NOW!");
  printf("\n\rTEST NOW!");
  
  while(!kbhit()) {
    if(Check4GlobalWarming()) {
      printf("\n\rTemperature Rises!");
    }
  }
  
  return 0;
}

#endif