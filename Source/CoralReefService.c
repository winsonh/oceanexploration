/****************************************************************************
 Module
   CoralReefService.c
 Description

 Author
   Winson Huang
****************************************************************************/

// #define TEST

/*----------------------------- Include Files -----------------------------*/
/* include header files for this service
*/
#include "CoralReefService.h"

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "ADMulti.h"
#include "PWM16Tiva.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

/* include header files for the other modules in the final project that are referenced
*/
#include "MasterService.h"


/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define NUM_AD_INPUT_PORTS 4
#define PWM_FREQ 50
#define PE1 1
#define WHITE_LED_PIN 4 
#define COLOR_LED_PIN 5
#define PWM_CHANNEL_GROUP 2
#define WHITE_AD_THRESOLD 3500 
#define FIRST_EVENT_THRESOLD 500
#define ZERO_VOLT_AD_OFFSET 150 // 70
#define BRIGHTNESS_RAMP_FACTOR 1000
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void ResetCoralReefService(void);
static void OutputPWM(uint8_t DutyCycle, uint8_t PinNum);
static void Celebrate(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static CoralReefState_t CurrentState = InitPState_CoralReef;
static ES_Event_t DeferralQueue[3+1];
static uint32_t ADResults[NUM_AD_INPUT_PORTS];
static uint16_t LastADCount = 0;
static bool CoralGameCompleted = false;
static bool SystemReset = false;
static bool OceanGameOn = false;
static bool LEAFIn = false;
/*------------------------------ Module Code ------------------------------*/





/****************************************************************************
 Function
     InitCoralReefSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description

 Author
     Winson Huang
****************************************************************************/
bool InitCoralReefSM ( uint8_t Priority )
{
  printf("InitCoralReefSM");
  ES_Event_t ThisEvent; 
  
  MyPriority = Priority;
	CurrentState = InitPState_CoralReef;
 
  // Set PWM Frequency
  PWM_TIVA_SetFreq(PWM_FREQ, PWM_CHANNEL_GROUP);
  
  // Set PWM Duty Cycle
  OutputPWM(0, WHITE_LED_PIN);
  OutputPWM(100, COLOR_LED_PIN);
     
  ThisEvent.EventType = ES_INIT;
  CurrentState = FullColor;
  
  printf("\n\rInitCoralReefService is successfully initialized.");
  return PostCoralReefSM(ThisEvent);
}



/****************************************************************************
 Function
     RunCoralReefSM

 Parameters
     ES_Event_t : an event sent to this service

 Returns
     ES_Event_t: ES_NO_EVENT if no error 

 Description

 Author
     Winson Huang
****************************************************************************/
ES_Event_t RunCoralReefSM(ES_Event_t ThisEvent) {
  struct ES_Event ReturnEvent;
  
   
  
  switch (CurrentState) {
    
    // Full Color State
    case FullColor:
      if(ThisEvent.EventType == ES_LEAF_INSERTION) {
        LEAFIn = true;
      }
    
      if(ThisEvent.EventType == ES_LEAF_REMOVAL) {
        OceanGameOn = false;
        LEAFIn = false;
        SystemReset = false;
        printf("\n\rCoralReefSM (FullColor state): receives ES_LEAF_REMOVAL");
        printf("\n\rCoralReefSM (FullColor state): sets OceanGameOn = false, LEAFIn = false, SystemReset = false\n");
      }

      if(ThisEvent.EventType == ES_GAME_ON) {
        OceanGameOn = true;
        LEAFIn = true;
        printf("\n\rCoralReefSM (FullColor state): receives ES_GAME_ON event");
        printf("\n\rCoralReefSM (FullColor state): starts OceanGameOn\n");
      }
      
      if(ThisEvent.EventType == ES_GAME_OVER) {
        OceanGameOn = false;
        SystemReset = false;
        printf("\n\rCoralReefSM (FullColor state): receives ES_GAME_OVER");
        printf("\n\rCoralReefSM (FullColor state): sets module variable OceanGameOn = false\n");
      }
      
      if(ThisEvent.EventType == ES_SYSTEM_RESET) {
        if(LEAFIn == false) {
          OutputPWM(0, WHITE_LED_PIN);
          OutputPWM(0, COLOR_LED_PIN);
          printf("\n\rCoralReefSM (FullColor state): receives ES_SYSTEM_RESET event (LEAFIn == false)");
        }
        else {
          OutputPWM(0, WHITE_LED_PIN);
          OutputPWM(100, COLOR_LED_PIN);
          printf("\n\rCoralReefSM (FullColor state): receives ES_SYSTEM_RESET event (LEAFIn == true)");
        }
        ResetCoralReefService();
        SystemReset = true;
        printf("\n\rCoralReefSM (FullColor state): receives ES_SYSTEM_RESET event");
        printf("\n\rCoralReefSM (FullColor state): resets the system\n");
      }  
      
      if((ThisEvent.EventType == ES_TEMPERATURE_RISE) && (OceanGameOn == true)) {
        CurrentState = ColorTransitioning;
        uint8_t WhiteDutyCycle = (uint8_t)(100.0 * (float)ThisEvent.EventParam / (float)WHITE_AD_THRESOLD);
        uint8_t ColorDutyCycle = 100 - WhiteDutyCycle;
        WhiteDutyCycle = 0.6 * WhiteDutyCycle;
        if(WhiteDutyCycle < 5) {
          WhiteDutyCycle = 0;
        }
        printf("\n\rWhiteDutyCycle = %d", WhiteDutyCycle);
        printf("\n\rColorDutyCycle = %d", ColorDutyCycle);
        
        // Output PWM for the current duty cycle
        OutputPWM(WhiteDutyCycle, WHITE_LED_PIN);
        OutputPWM(ColorDutyCycle, COLOR_LED_PIN);
      }
      break;
    
    
    // Color Transitioning State
    case ColorTransitioning:
      
      if(ThisEvent.EventType == ES_LEAF_REMOVAL) {
        OceanGameOn = false;
        LEAFIn = false;
        SystemReset = false;
        printf("\n\rCoralReefSM (ColorTransitioning state): receives ES_LEAF_REMOVAL");
        printf("\n\rCoralReefSM (ColorTransitioning state): sets OceanGameOn = false, LEAFIn = false, SystemReset = false\n");
      }

      if(ThisEvent.EventType == ES_GAME_OVER) {
        OceanGameOn = false;
        SystemReset = false;
        printf("\n\rCoralReefSM (ColorTransitioning state): receives ES_GAME_OVER event");
        printf("\n\rCoralReefSM (ColorTransitioning state): sets OceanGameOn = false, LEAFIn = false, SystemReset = false\n");
      }
      
      if(ThisEvent.EventType == ES_SYSTEM_RESET) {
        if(LEAFIn == false) {
          OutputPWM(0, WHITE_LED_PIN);
          OutputPWM(0, COLOR_LED_PIN);          
        }
        else {
          OutputPWM(0, WHITE_LED_PIN);
          OutputPWM(100, COLOR_LED_PIN);
        }
        ResetCoralReefService();
        SystemReset = true;
        printf("\n\rCoralReefSM (ColorTransitioning state): receives ES_SYSTEM_RESET event");
        printf("\n\rCoralReefSM (ColorTransitioning state): resets the system\n");
      }
      
      if((ThisEvent.EventType == ES_TEMPERATURE_RISE) && (OceanGameOn == true)) {
        uint8_t WhiteDutyCycle = (uint8_t)(100.0 * (float)ThisEvent.EventParam / (float)WHITE_AD_THRESOLD);
        uint8_t ColorDutyCycle = 100 - WhiteDutyCycle;
        WhiteDutyCycle = 0.6 * WhiteDutyCycle;
        if(WhiteDutyCycle < 5) {
          WhiteDutyCycle = 0;
        }
        // printf("\n\rWhiteDutyCycle = %d", WhiteDutyCycle);
        // printf("\n\rColorDutyCycle = %d", ColorDutyCycle);
                
        // If white duty cycle > 50%, enter White state
        if(WhiteDutyCycle >= 50) {
          if(CoralGameCompleted == false) {

            
            OutputPWM(100, WHITE_LED_PIN);
            OutputPWM(0, COLOR_LED_PIN);
            Celebrate();
            
            struct ES_Event CORALREEF_WIN_Event;
            CORALREEF_WIN_Event.EventType = ES_CORALREEF_GAME_WIN;
            PostMasterSM(CORALREEF_WIN_Event); // Post to the main state machine
            
            CoralGameCompleted = true;
            CurrentState = White; 
            
            printf("\n\rCoralReefSM (ColorTransitioning state): WhiteDutyCycle >=100");
            printf("\n\rCoralReefSM (ColorTransitioning state): posts ES_CORALREEF_GAME_WIN to MasterSM");
            printf("\n\rCoralReefSM (ColorTransitioning state): sets CoralGameCompleted = true");
            printf("\n\rCoralReefSM (ColorTransitioning state): enters White state\n");
            
            break;
          }
        }     
        // Output PWM for the current duty cycle
        OutputPWM(WhiteDutyCycle, WHITE_LED_PIN);
        OutputPWM(ColorDutyCycle, COLOR_LED_PIN);
      }
      
      break;
      
      
    case White:
      if(ThisEvent.EventType == ES_LEAF_REMOVAL) {
        OceanGameOn = false;
        LEAFIn = false;
        SystemReset = false;
        printf("\n\rCoralReefSM (White state): receives ES_LEAF_REMOVAL");
        printf("\n\rCoralReefSM (White state): sets OceanGameOn = false, LEAFIn = false, SystemReset = false\n");
      }
      
      if(ThisEvent.EventType == ES_SYSTEM_RESET) {
        if(LEAFIn == false) {
          OutputPWM(0, WHITE_LED_PIN);
          OutputPWM(0, COLOR_LED_PIN);          
        }
        else {
          OutputPWM(0, WHITE_LED_PIN);
          OutputPWM(100, COLOR_LED_PIN);
        }
        ResetCoralReefService();
        SystemReset = true;
        printf("\n\rCoralReefSM (White state): receives ES_SYSTEM_RESET");
        printf("\n\rCoralReefSM (White state): resets the system\n");
      }
      
      if(ThisEvent.EventType == ES_GAME_OVER) {
        OceanGameOn = false;
        SystemReset = false;
        printf("\n\rCoralReefSM (White state): receives ES_GAME_OVER");
        printf("\n\rCoralReefSM (White state): sets OceanGameOn = false, SystemReset = false\n");
      }
      break;
  }
  ReturnEvent.EventType = ES_NO_EVENT;
  return ReturnEvent;
}



/****************************************************************************
 Function
     Check4TemperatureEvent

 Parameters
     None

 Returns
     bool, true if a temperature event is detected, false otherwise

 Description

 
 Author
     Winson Huang
****************************************************************************/
bool Check4TemperatureEvent(void) {
  
  bool ReturnVal = false;
  uint16_t CurrentADCounts = 0;
  
  // Update ADResults with the current analog inputs, PE1 = ADResults[1]
  ADC_MultiRead(ADResults); 
  CurrentADCounts = ADResults[PE1]; 
  // printf("\n\rCurrentADCounts = %d", CurrentADCounts);
  
  
  // If a rise in temperature is detected
  if ((((CurrentADCounts - LastADCount) >= 200) || ((LastADCount - CurrentADCounts) >= 200)) && (LEAFIn == true) && (OceanGameOn == true)) { // ( (CurrentADCounts > LastADCount) && (LEAFIn == true) && (OceanGameOn == true)) {
    ReturnVal = true;
    
    // printf("\n\rNew LastADCount = %d", LastADCount);
    
    // When a rise temperature is detected, reset and restart the 30-s timer.
    if((CurrentADCounts - LastADCount) >= 210) {
      ES_Timer_StopTimer(TIMER_30s);
      ES_Timer_SetTimer(TIMER_30s, 30 * ONE_SEC);
      ES_Timer_StartTimer(TIMER_30s);
      printf("\n\rCoralReefService: CurrentADCounts = %d, LastADCount = %d, 30s Timer resets.\n", CurrentADCounts, LastADCount);
    }
    
    LastADCount = CurrentADCounts;
     
    // Post ES_TEMPERATURE_RISE Event to CoralReefService
    ES_Event_t ThisEvent;
    ThisEvent.EventType = ES_TEMPERATURE_RISE; 
    ThisEvent.EventParam = CurrentADCounts;
    PostCoralReefSM(ThisEvent); 
  }
  else if ((CurrentADCounts <=  ZERO_VOLT_AD_OFFSET) && (LEAFIn == false) && (SystemReset == false)){
    ReturnVal = true;
    
    // Post ES_TEMPERATURE_RISE Event to CoralReefService
    ES_Event_t RTD_ROOM_TEMP_Event;
    RTD_ROOM_TEMP_Event.EventType = ES_RTD_AT_ROOM_TEMP; 
    PostMasterSM(RTD_ROOM_TEMP_Event);
    printf("\n\r Sent ES_RTD_AT_ROOM_TEMP");
  }
  
  return ReturnVal;
}



/****************************************************************************
 Function
     PostCoralReefSM

 Parameters
     EF_Event: an event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue

 Author
     Winson Huang
****************************************************************************/
bool PostCoralReefSM( ES_Event_t ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}


/****************************************************************************
 Function
     ResetCoralReefService

 Parameters
     None

 Returns
     None

 Description

 Author
     Winson Huang
****************************************************************************/
static void ResetCoralReefService(void) {
  LastADCount = FIRST_EVENT_THRESOLD;
  CoralGameCompleted = false;
  CurrentState = FullColor;
  OceanGameOn = false;
  LEAFIn = false;
        
  // Set Initial PWM Duty Cycle
  /*
  OutputPWM(0, WHITE_LED_PIN);
  OutputPWM(100, COLOR_LED_PIN);
  */

}






/****************************************************************************
 Function
     OutputPWM

 Parameters
     uint8_t: duty cycle for PWM
     uint8_t: pin number of PWM output

 Returns
     None

 Description

 Author
     Winson Huang
****************************************************************************/
static void OutputPWM(uint8_t DutyCycle, uint8_t PinNum) {
  
  if(PWM_TIVA_SetDuty( DutyCycle, PinNum) != true){
    printf("\n\rFailed PWM_TIVA_SetDuty( %d, %d)", DutyCycle, PinNum);
    return;
  }
  
  // printf("\n\rPWM_TIVA_SetDuty( %d, %d)", DutyCycle, PinNum);
}



/****************************************************************************
 Function
     Celebrate

 Parameters
     None

 Returns
     None

 Description

 Author
     Winson Huang
****************************************************************************/
static void Celebrate(void) {
  
  for(int j = 0; j <= 2; j++) {
    for(int i = (100 * BRIGHTNESS_RAMP_FACTOR); i >= 0; i--) {
      // printf("\n\rCelebrate is called");
      OutputPWM((i/BRIGHTNESS_RAMP_FACTOR), WHITE_LED_PIN);
    }
    
    for(int i = 0; i <= (100 * BRIGHTNESS_RAMP_FACTOR); i++) {
      // printf("\n\rCelebrate is called");
      OutputPWM((i/BRIGHTNESS_RAMP_FACTOR), WHITE_LED_PIN);
    }
  }
}















































/****************************************************************************
                                Test Harness
****************************************************************************/
#ifdef TEST
#include <termio.h>

int main(void) {
  TERMIO_Init();
  // printf("TEST NOW!\n");
  InitCoralReefService (MyPriority);
  
  printf("\n\rTEST NOW!");
  printf("\n\rTEST NOW!");
  printf("\n\rTEST NOW!");
  printf("\n\rTEST NOW!");
  
  while(!kbhit()) {
    if(Check4TemperatureEvent()) {
      printf("\n\rTemperature Rises!");
    }
  }
  
  return 0;
}

#endif